FROM php:7.0-alpine

ENV COMPOSER_ALLOW_SUPERUSER 1
RUN echo "date.timezone=UTC" >> $PHP_INI_DIR/php.ini
RUN echo "short_open_tag=0" >> $PHP_INI_DIR/php.ini

RUN apk --no-cache add icu-dev \
 && docker-php-ext-install intl opcache \
 && apk --no-cache add --virtual .builddeps $PHPIZE_DEPS \
 && pecl install xdebug && docker-php-ext-enable xdebug \
 && apk del .builddeps

RUN curl -Ls https://getcomposer.org/composer.phar > /usr/local/bin/composer && chmod +x /usr/local/bin/composer \
 && curl -Ls https://phar.phpunit.de/phpunit.phar > /usr/local/bin/phpunit && chmod +x /usr/local/bin/phpunit


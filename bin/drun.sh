#!/bin/bash

set -e
set -u
set -o pipefail

COMMAND=${1:-"help"}

export SYMFONY_ENV=dev
export SYMFONY_DEBUG=1

function build() {
    docker build -t hangman/php:7.0 .
}

function run() {
    command=${@}
    if [ "" == "$command" ]; then
      command=sh
    fi
    docker run -it --rm -v $(pwd):/hangman -v $HOME/.composer/cache:/root/.composer/cache -w=/hangman hangman/php:7.0 $command
}

function up() {
    docker run -it --rm -v $(pwd):/hangman -v $HOME/.composer/cache:/root/.composer/cache -w=/hangman -p 8000:8000 hangman/php:7.0 bin/console server:run 0.0.0.0:8000 "$@"
}

function phpunit() {
    run phpunit ${@:-""}
}

function php() {
    run php ${@:-"-v"}
}

function sh() {
    run ${@:-"sh"}
}

function composer() {
    run composer ${@:-""}
}

function help() {
    USAGE="$0 "$(compgen -A function | tr "\\n" "|" | sed 's/|$//')
    echo $USAGE
}

if [ "$(type -t $COMMAND)" != "function" ]; then
    help
    exit 1
fi

$COMMAND ${@:2}
